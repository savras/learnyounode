// To test, node 03.myFirstIO.js F:\Source\NodeJS.Project\03.myFirstIOFile.txt
(function () {
    'use strict';
    
    var filename =  process.argv[2];    // This starts with 2 because process.argv = ['node', '03.myFirstIO.js', directory .etc]
    
    var fs = require('fs');
    var array = fs.readFileSync(filename).toString().split('\n');
    //var array = fs.readFileSync(filename, 'utf8').split('\n'); // will return a string as well.

    console.log(array.length - 1);
})();