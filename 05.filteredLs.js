/*******************************************************
    Node answer
    ===========
    var fs = require('fs')
    var path = require('path')

    fs.readdir(process.argv[2], function (err, list) {
      list.forEach(function (file) {
        if (path.extname(file) === '.' + process.argv[3])
          console.log(file)
      })
    })
********************************************************/
(function (){
    'use strict';
    
    var period = '.';
    var filePath = process.argv[2];
    var extension = period + process.argv[3];
    
    var fs = require('fs');    
    fs.readdir(filePath, callback);    
        
    function callback(error, fileNames) {
        if(!error) {
            for(var i = 0; i < fileNames.length; i++) {
                if(endsWith(fileNames[i], extension))
                {
                    console.log(fileNames[i]);
                }
            }
        }       
    }
    
    function endsWith(stringToMatch, match) {
        var offset = 1;
        var index = stringToMatch.indexOf(match);
        var foundIndex = index - offset;
        var actualIndex = stringToMatch.length - offset - match.length;
        return (index > 0) && (foundIndex === actualIndex);  
    }
})();