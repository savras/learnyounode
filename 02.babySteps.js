// To run, call node 02.babySteps.js a b c d

// Node.js answer
// It assumes that one invalid input will return NaN even if the other inputs are valid.
// Number('Not a Number') + +1 is NaN

//var result = 0;
//for(var i = 2; i < process.argv.length; i++) {
//    result += Number(process.argv[i]);
//}
    
//console.log(result);

// Own answer
console.log('Processing input: ' + process.argv);

var length = process.argv.length;
var sum = 0;
for(var i = 0; i < length; i++) {
    if(i == 0 || i == 1) {
        continue;
    }
    else {
        if(isNaN(parseInt(process.argv[i], 10))) {
            //console.log('Input is not an integer');
        }
        else {                
            sum += parseInt(process.argv[i], 10);
        }
    }
}

console.log(sum);