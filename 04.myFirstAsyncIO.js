// To test, node 04.myFirstAsyncIO.js F:\Source\NodeJS.Project\03.myFirstIOFile.txt
(function (){
    'use strict';
    
    var filePath = process.argv[2];
    
    var fs = require('fs');
    //console.log('After require fs...');
    
    fs.readFile(filePath, 'utf8', callback);    
        
    function callback(error, data) {
        //console.log('In callback...');
        if(!error)
        {            
            console.log(data.split('\n').length -1);            
        }
        //console.log(error);
    }    
})();